## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_purpose Purpose
#  In this lab, the main purpose was to use the motor encoder to track and record the change in rotational displacement
#  To do this, the encoder must be powered with 5 V, and the A and B phase must be connected to a timer.  
#  The timer is then able to differentiate between the two phases because they are in quadrature, which allows
#  detection of motion in either direction.
#
#  @section sec_usage Usage
#  Furthermore, through using object attributes, the code is able
#  to handle multiple encoders at once. It does that by storing the encoders positional data
#  as an attribute to the created encoder object. The data can then by access and updated based on
#  how the encoder moves.
#
#  @section sec_testing Testing
#  In order to ensure the encoders were working properly, they were tested manually to see if their values were
#  accurate. This was done by manually spinning the motors and checking if the position and delta of the encoder are
#  updated correctly.  I also tested one of the encoders while the motor was running to ensure higher speeds did not
#  mess up the values. 
#
#
#  @section sec_bugs Bugs and Limitations
#  Through my testing, I have came upon a few critical bugs that still exist.  The encoders will be working fine
#  then all the sudden it will be unresponsive randomly.
#  For example, if I allow the motor to run and measure the delta, it only works sometimes.  It will either give
#  me what I believe to be there correct value, or it will return a value of -1 or 1. This happens randomly as far as I can tell.
#  Furthermore, my second encoder can sometimes not update a negative value.  Again this only happens some of the time and
#  the rest of the time, the functionality works. Also because the code for encoders one and two are nearly identical,
#  it leads me to believe there might be something wrong with the encoder or board. 
#
#  @section sourcecode Sourcecode 
#  Sourcode can be found @ https://bitbucket.org/andrewmeyenberg/me405_labs/src/master/Lab_2/Lab_2.py 
#  
#
#  @author Andrew Meyenberg
#
#  @copyright 2020 by Andrew Meyenberg
#
#  @date April 24, 2020
#