''' @file lab_0.py 
This file serves to perform a caculation of a Fibonacci number
based on the users input. It demonstrates basic user interface and 
error handling. '''


import sys
'''must import sys module to exit file when program is done'''

"""
Fibonacci function based on code from:
    Title: Python Program for n-th Fibonacci number
    Author: Sakey Modi
    Date: 4/17/20
    Code Version: Driver Version
    Availability: https://www.geeksforgeeks.org/python-program-for-n-th-fibonacci-number/
"""      
def fib(idx):
   '''Based on the value idx inputted by the user, this fucntion will 
   solve for the corresponding Fibonacci number and returns the results'''
   if idx == 0:
       return 0
   elif idx == 1:
       return 1
   else:
       return fib(idx-1) + fib(idx-2)
 
## String containing user input to be check if it is a positive integer to 
#  to be used to find the Fibonacci number    
user_in = input('Enter a positve integer to calculate'
                ' Fibonnaci number or (exit) to leave: ')

    
if __name__ == '__main__':
     while(True):
         if user_in.isdigit():
             print ('Calculating Fibonacci number at ' 
                    'index n =',(user_in))  
             print(fib(int(user_in)))
         elif user_in == 'exit':
              sys.exit('Thanks for using the Fibonacci calculator')
         else:
             print('invalid input')
    
         user_in = input('Enter another positve index integer to calculate'
                        'Fibonnaci number or (exit) to leave:  ')