## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  In this lab, the main goal was to use the motor driver in conjuction with our main board to drive
#  a motor.  To do this, pulse width modulation was used to drive the motor.  In order to drive it in
#  reverse, the PWM and negative end of the motor were programed to switch.  All of this is handle, as well as
#  user input and error messages so a user may input a duty cycle into the console.
#  Source code can be found @ https://bitbucket.org/andrewmeyenberg/me405_labs/src/master/Lab_1/Lab_1.py
#
#  @section sec_mot Motor Driver
#  The motor driver being used to drive the \ref motor in this lab is the X-NUCLEO-IMH04A1.  It is capable
#  of driving two brushed dc motors.  More information on the motor driver can be found @
#  https://www.mouser.com/ProductDetail/STMicroelectronics/X-NUCLEO-IHM04A1?qs=%2Fha2pyFadugfOa1q%2FRFISeT05v4cFA%2Fg264X%2Ft7U2I%2F1jP900NOOjg%3D%3D
#
# 
#
#  @author Andrew Meyenberg
#
#  @copyright 2020 by Andrew Meyenberg
#
#  @date April 24, 2020
#