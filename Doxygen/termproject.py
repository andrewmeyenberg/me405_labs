## @file termproject.py
#
#  @section sec_purpose Purpose
#  The purpose of this term project was to design and build a device that a user can interact with the enviorment through
#  sensors and motors previously used in the class. My device was designed to be a 2 degrees of freedom laser pointer. This
#  was accomplished by connected the two motors together to allow for a full spherical range.  The user can the manipulate
#  the gravity sensor to change the direction the laser is pointing. 
#
#  @section sec_usage Usage
#  The main method of code orginization was classes.  The four classes used were the Motordriver, Encoder, Pcontroller, and bno055.
#  In order to interact with sensor, I2C protocols were followed. This allows for the memory
#  of the BNO055 to be written to and read. From there, the euler angle values were passed through to the Pcontroller, were
#  a duty cycle is assigned to reach that given angle.
#   
#
#  @section sec_testing Testing
#  Testing was done throughtout the design process to ensure the motors were moving in the correct directions.  Once the motors were connected
#  and the laser pointer was mounted, I began to test its accuracy and how it interacted with the wiring. I would slowly turn the sensor a full
#  rotation and see if the wires become a problem. To combat the wires tangling, I extended the second motor's wiring another 8 inches or so. This
#  allowed for the motor to fully rotate freely.
#
#  @section sec_vid Video Demonstration
#  Below is a video showing the two degree of freedom laser pointer in action.
#  https://drive.google.com/file/d/1FIwj6H_micZfynjosJzd9iJxJnF3megT/view?usp=sharing
#
#  @section sec_bugs Bugs and Limitations
#  Through my testing, I noticed there were no serious bugs that I found.  The laser pointer followed where I was pointing with the sensor in a nearly full
#  spherical range.  The device was very responsive to quick movements, but was not great at small movements.  This is most likely caused by the way I move the
#  motors using a proportional controller.  Additionally, the friction in the gearbox adds to the problem of in accurate small movements, as the duty cycle must be set at least
#  12 to overcome the static friction.
#
#  @section sourcecode Sourcecode 
#  Sourcode can be found @ https://bitbucket.org/andrewmeyenberg/me405_labs/src/master/Doxygen/termproject.py
#
#  @author Andrew Meyenberg
#
#  @date June 8, 2020
#
import pyb
import sys
from pyb import I2C
import utime
import ustruct

pin_ENa = pyb.Pin (pyb.Pin.cpu.A10, pyb.Pin.OUT_PP)
pin_IN1a = pyb.Pin (pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)
pin_IN2a = pyb.Pin (pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)
pin_ENb = pyb.Pin (pyb.Pin.cpu.C1, pyb.Pin.OUT_PP)
pin_IN1b = pyb.Pin (pyb.Pin.cpu.A0, pyb.Pin.OUT_PP)
pin_IN2b = pyb.Pin (pyb.Pin.cpu.A1, pyb.Pin.OUT_PP)


tim3 = pyb.Timer(3, freq = 20000)
t3ch1 = tim3.channel(1, pyb.Timer.PWM, pin=pin_IN1a)
t3ch2 = tim3.channel(2, pyb.Timer.PWM, pin=pin_IN2a)
tim5 = pyb.Timer(5, freq = 20000)
t5ch1 = tim5.channel(1, pyb.Timer.PWM, pin=pin_IN1b)
t5ch2 = tim5.channel(2, pyb.Timer.PWM, pin=pin_IN2b)

i2c = pyb.I2C(1, pyb.I2C.MASTER)
i2c.init(I2C.MASTER)
utime.sleep_ms(10)

# Includes the pin objects used for interfacing with the the encoders.  Pins B6 and B7
# correspond to the first encoder and pins C6 and C7 correspond to the second encoder
pin_B6 = pyb.Pin (pyb.Pin.cpu.B6, pyb.Pin.AF_PP, af=2)
pin_B7 = pyb.Pin (pyb.Pin.cpu.B7, pyb.Pin.AF_PP, af=2)
pin_C6 = pyb.Pin (pyb.Pin.cpu.C6, pyb.Pin.AF_PP, af=3)
pin_C7 = pyb.Pin (pyb.Pin.cpu.C7, pyb.Pin.AF_PP, af=3)

# Includes the timer objects used for interfacing with the the encoders. Tim4
# correspond to the first encoder's timer and Tim8 correspond to the second encoder's timer
tim4 = pyb.Timer(4,prescaler=0, period=65535)
tim8 = pyb.Timer(8,prescaler=0, period=65535)
t4ch1 = tim4.channel(1, pyb.Timer.ENC_A, pin=pin_B6)
t4ch2 = tim4.channel(2, pyb.Timer.ENC_B, pin=pin_B7)
t8ch1 = tim8.channel(1, pyb.Timer.ENC_A, pin=pin_C6)
t8ch2 = tim8.channel(2, pyb.Timer.ENC_B, pin=pin_C7)

# creates an I2C object to interact with the sensor. Then
# initilized and waits 10 ms to avoid any startup problems
i2c = pyb.I2C(1, pyb.I2C.MASTER)
i2c.init(I2C.MASTER)
utime.sleep_ms(10)

result = []
ang = []
'''an array to store calibration results'''

## A Motor Driver object
#
#  A class used to assign attributes of the motor
#  @author Your Name
#  @copyright License Info
#  @date January 1, 1970
class MotorDriver:
    
    ## Constructor for motor driver
    #
    #  Creates a motor driver by initializing GPIO 
    #  pins and turning the motor off for safety.
    #  @param EN_pin A pyb.Pin object to use as the enable pin.
    #  @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
    #  @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
    #  @param timer A pyb.Timer object to use for PWM generation on IN1_pin
    #  and IN2_pin. 
    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer,ch1,ch2):
        self.EN_pin = EN_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = timer
        self.ch1 = ch1
        self.ch2 = ch2
    print ('Creating a motor driver')
   
    ## Enables the motor 
    #
    #  Enables the motor to run by setting the EN pin high
    def enable (self):
        self.EN_pin.high ()
        print ('Enabling Motor')

    ## Disables the motor and exits program
    #
    #  In order to disable the motor, the EN pin is set to low. Then a
    #  system exit occurs to end the code when a leave is entered
    def disable (self):
        print('Disabling Motor')
        self.EN_pin.low ()
        print('Thanks for using the Motor Driver')
        sys.exit()

    ## Sets the duty cycle of the motor
    #
    #  This method sets the duty cycle to be sent
    #  to the motor to the given level. Positive values
    #  cause effort in one direction, negative values
    #  in the opposite direction.  If the absolute value of a
    #  duty cycle is greater than 100, it will be lowered
    #  to 100 and then set.
    #  @param duty A signed integer holding the duty
    #  cycle of the PWM signal sent to the motor    
    def set_duty (self, duty):
        if duty >= 20:
            duty = 20
            self.IN2_pin.low ()
            self.ch1.pulse_width_percent(int(duty))
            self.ch2.pulse_width_percent(0)
            print('Duty cycle updated to: ' + str(duty))
        elif duty >= 0:
            self.IN2_pin.low ()
            self.ch1.pulse_width_percent(int(duty))
            self.ch2.pulse_width_percent(0)
            print('Duty cycle updated to: ' + str(duty))
        elif duty <= -20:
            duty = -20
            self.IN1_pin.low ()
            self.ch2.pulse_width_percent(int(duty*-1))
            self.ch1.pulse_width_percent(0)
            print('Duty cycle updated to: ' + str(duty))
        else:
            self.IN1_pin.low ()
            self.ch2.pulse_width_percent(int(duty*-1))
            self.ch1.pulse_width_percent(0)
            print('Duty cycle updated to: ' + str(duty))
'''above the duty cycle is limited to 20 percent, as faster speeds are not neccesary and
    could potentially damage the hardware'''


class Encoder:
    
    ## Constructor for Encoder 
    #
    #  Creates a encoder driver by initializing the timer 
    #  pins a creating attributes to store data
    #  @param pin_A  A pyb.Pin object to be connected to the A phase of the encoder
    #  @param pin_B  A pyb.Pin object to be connected to the B phase of the encoder 
    #  @param timer A pyb.Timer object to use for reading the encoder values 
    #  @param name An attribute of the encoder to be used for naming specific encoders
    #  @param delta  An attribute describing the change in rotation of the encoder between time intervals
    #  @param position An attribute describing the total rotation of the encoder
    #  @param xold An attribute used to hold the position of the encider from the previous time interval to calculate delta
    #  @param xnew An attrubute used to hold the new postion on the encoder in order to calculate delta
    def __init__ (self, pin_A, pin_B, timer, name, delta, position, xold, xnew):
        self.Pin_A = pin_A
        self.pin_B = pin_B
        self.timer = timer
        self.name = name
        self.delta = delta
        self.position = position
        self.xold = xold
        self.xnew = xnew
        print ('Encoder Inititialized')

    ## Updates the encoder 
    #
    #  First, the previous xnew is saved as xold, then the xnew is updated by the timer of that encoder.
    def update(self):
        self.xold = self.xnew
        timer = getattr(self, 'timer') 
        self.xnew = timer.counter()

    ## Obtains the current position of the encoder
    #
    #  The position is udpated ever time interval in order to obtain the total rotational displacement of the encoder.
    #  It does this by taking the old position of the encoder and adding the delta to it. 
    def get_position(self):
        self.position = self.position + self.delta

 
    ## Manually sets the position value of the encoder
    #
    #  The position value is udpated to the specified new position. The motor itself does not move to the position
    #  @param new_position A user specified integer value to become the new current position of the encoder 
    def set_position(self, new_position):
            self.position = new_position

    ## Obtains the change in position, delta
    #
    #  The position is udpated ever time interval in order to obtain the total rotational displacement of the encoder.
    #  It does this by taking the old position of the encoder and adding the delta to it.         
    def get_delta(self):
            self.delta = self.xnew - self.xold
            if self.delta >= 32768:
                self.delta = self.delta - 65535 
            elif self.delta <= -32768:
                self.delta = self.delta + 65535
            else:
                pass         
'''above, is the overflow protection for the encoder.  It ensures that when the encoder overflows,
   it is offset by the max value to get the correct value'''


## A Proportional Controller object
#
#  A class used to assign attributes of the Proportional Controller
#  @author Andrew Meyenberg
#  @date May 13, 2020
class Pcontroller:
    
    ## Constructor for proportional controller 
    #
    #  Creates a proportional controller  by 
    #  creating attributes to store data
    #  @param Kp An attribute of the controller that stores the gain for the test
    #  @param setpoint  An attribute to store the setpoint of the gain test
    def __init__ (self, Kp, encoder, motor):
        self.Kp = Kp
        self.encoder = encoder
        self.motor = motor
        print('Proportional controller intialized')
    
    ## Updates the proportional controller and duty cycle of the motor
    #
    #  First, the position of the encoder is updated. then the delta is found and added to the
    #  total position. Following that, the error from the setpoint is calculated and multiplied
    #  by the gain to set the duty cycle.  
    def update(self,setpoint):
        self.encoder.update()
        self.encoder.get_delta()
        self.encoder.get_position()
        err = setpoint - self.encoder.position
#         print('postion =' + str(self.encoder.position))
#         print('err =' + str(err))
        if self == pc1:
            duty = (err*Kp)/1000
        else:
            duty = (-err*Kp)/1000
        if duty <= 12 and duty >= 1:
            duty = 12
        elif duty >= -12 and duty <= -1:
            duty = -12
        else:
            duty = duty
        self.motor.set_duty(int(duty))
'''above is an method of saturating the duty cycle to 12 when it is below 12.  This is necessary because
    the motors' friction does not allow movement under a duty cycle of 12'''

## A BNO055 IMU object
#
#  A class used to initilize and control the BNO055 IMU
#  @author Andrew Meyenberg
#  @date May 20, 2020
class bno055:
    
    ## Constructor for the BNO055 IMU
    #
    #  Creates a BNO055 IMU class by initializing the sensor
    #  through I2C. As there is only one device, the BNO055 is declared master. 
    def __init__ (self):
        i2c.init(I2C.MASTER)
        print('BNO055 initilized')
    
    ## Enables the sensor 
    #
    #  Enables the sensor by setting the operating mode of the sensor to NDOF.
    #  NDOF stand for nine degrees of freedom and allows for data to be
    #  collected from the accelerometer, gyroscope and the magnetometer.   
    def enable(self):
        i2c.mem_write(0xC, 0x28, 0x3D, timeout=1000)    #sets operating mode to NDOF 
        print('BNO055 enabled')
        
    ## Disables the sensor 
    #
    #  Disables the sensor by setting it's operating mode back to the default
    #  of config mode.  
    def disable(self):
        i2c.mem_write(0xC, 0x28, 0x00, timeout=1000)    #sets operating mode to configmode 
        print('BNO055 disabled')   
    
    ## Retrieves the Euler angle values.
    #
    #  To do this, the 6 bytes of memory storing the Euler angle values are read.  Then
    #  the 6 bytes are unpacked into 3 numbers corresponding to the 3 axes. These are then
    #  divided by 16 to fix the resolution to degrees. The final Euler angles are returned as a tuple.
    def get_angles(self):
            global ang
            data = i2c.mem_read(6, 0x28,0x1A)  
            angles = ustruct.unpack('<hhh',data)
            ang = tuple(i/16 for i in angles)
            print('Eul Ang:' + str(tuple(i/16 for i in angles)))
            
            
            
    ## Retrieves the Euler velocity values.
    #
    #  To do this, the 6 bytes of memory storing the Euler velocity values are read.  Then
    #  the 6 bytes are unpacked into 3 numbers corresponding to the 3 axes. These are then
    #  divided by 16 to fix the resolution to degrees/second. The final Euler velocities are returned as a tuple.    
    def get_ang_vel(self):
            data = i2c.mem_read(6, 0x28,0x14)  
            ang_vel = ustruct.unpack('<hhh',data)
            ang = tuple(i/16 for i in ang_vel)
            print('Eul Vel:' + str(tuple(i/16 for i in ang_vel)))
    
            
# Create a motor object passing in the pins and timer
moe1 = MotorDriver(pin_ENa, pin_IN1a, pin_IN2a, tim3, t3ch1, t3ch2)
moe2 = MotorDriver(pin_ENb, pin_IN1b, pin_IN2b, tim5, t5ch1, t5ch2)
# Enable the motor driver
moe1.enable()
moe2.enable()
# moe2.enable()


# Set the duty cycle to 10 percent
moe1.set_duty(0)
moe2.set_duty(0)

# Create the two encoder objects to be tested.  Position are set to arbitrary initial values to test.
enc1 = Encoder(pin_B6, pin_B7, tim4,'Encoder 1',0,0,0,0)
enc1.set_position(0)
enc2 = Encoder(pin_C6, pin_C7, tim8,'Encoder 2',0,0,0,0)
enc2.set_position(0)
utime.sleep_ms(500)

Kp = 100
pc1 = Pcontroller(Kp,enc1, moe1)
pc2 = Pcontroller(Kp, enc2, moe2)

#  Creaing the sensor object and then enabling it for use. 
sensor = bno055()
sensor.enable()
enc1.set_position(530) #adjust horizontal axis. greater than 600 is CCW; less than 600 is CW
enc2.set_position(-20)   #adjust vertical axis once per startup. + is up, - is down

if __name__ == '__main__':
    while(True):
        utime.sleep_ms(10)
        sensor.get_angles()
        pc1.update(ang[0]*700/360)
        pc2.update(ang[1]*700/360)