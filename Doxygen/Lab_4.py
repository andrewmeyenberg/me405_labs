## @file Lab_4.py
#
#  @section sec_purpose Purpose
#  In this lab, the main purpose was to interact with the BNO055 IMU. To show that the IMU was
#  orperational, the Euler angles and velocities, as well as the calibration results were displayed.
#  This allows the user to manipulate the IMU and record its precise movements. 
#
#  @section sec_usage Usage
#  The main method of code orgainazition for this lab was a single class for the BNO055.
#  In order to interact with sensor, I2C protocols were followed. This allows for the memory
#  of the BNO055 to be written to and read. This is vital in setting up the sensor's
#  mode and reading its euler values.
#   
#
#  @section sec_testing Testing
#  In order to ensure the IMU was working properly, I manually tested the Euler angles by testing each axis at 90 degrees.
#  This ensures that each axis is working reasonibly well.  As for the Euler velocities, I was
#  not able to test it beyond just turning the device quickly and ensuring a velocity was measured. The video linked below
#  shows how I tested the device's heading, which is the first Euler angle listed.
#
#
#  @section sec_video IMU Testing Video
#  https://drive.google.com/file/d/1sMTIKYxzEps5jcIQ3ZuN0B9k_zz3qr8R/view?usp=sharing
#
#  @section sec_bugs Bugs and Limitations
#  Through my testing, I was unable to find any bugs or limitations to the device. It seems both very accurate
#  and very capable in handling multiple sensing tasks.
#
#  @section sourcecode Sourcecode 
#  Sourcode can be found @ https://bitbucket.org/andrewmeyenberg/me405_labs/src/master/Doxygen/Lab_4.py
#
#  @author Andrew Meyenberg
#
#  @date May 20, 2020
#
import pyb
from pyb import I2C
import utime
import ustruct
'''must import pyb module to to interface with board
   must import I2C to interface with gravity sensor
   must import utime module to add a predictable delay
   must import ustruct to handle bytes'''

# creates an I2C object to interact with the sensor. Then
# initilized and waits 10 ms to avoid any startup problems
i2c = pyb.I2C(1, pyb.I2C.MASTER)
i2c.init(I2C.MASTER)
utime.sleep_ms(10)


result = []
'''an array to store calibration results'''

## A BNO055 IMU object
#
#  A class used to initilize and control the BNO055 IMU
#  @author Andrew Meyenberg
#  @date May 20, 2020
class bno055:
    
    ## Constructor for the BNO055 IMU
    #
    #  Creates a BNO055 IMU class by initializing the sensor
    #  through I2C. As there is only one device, the BNO055 is declared master. 
    def __init__ (self):
        i2c.init(I2C.MASTER)
        print('BNO055 initilized')
    
    ## Enables the sensor 
    #
    #  Enables the sensor by setting the operating mode of the sensor to NDOF.
    #  NDOF stand for nine degrees of freedom and allows for data to be
    #  collected from the accelerometer, gyroscope and the magnetometer.   
    def enable(self):
        i2c.mem_write(0xC, 0x28, 0x3D, timeout=1000)    #sets operating mode to NDOF 
        print('BNO055 enabled')
        
    ## Disables the sensor 
    #
    #  Disables the sensor by setting it's operating mode back to the default
    #  of config mode.  
    def disable(self):
        i2c.mem_write(0xC, 0x28, 0x00, timeout=1000)    #sets operating mode to configmode 
        print('BNO055 disabled')   
    
    ## Retrieves the Euler angle values.
    #
    #  To do this, the 6 bytes of memory storing the Euler angle values are read.  Then
    #  the 6 bytes are unpacked into 3 numbers corresponding to the 3 axes. These are then
    #  divided by 16 to fix the resolution to degrees. The final Euler angles are returned as a tuple.
    def get_angles(self):     
            data = i2c.mem_read(6, 0x28,0x1A)  
            angles = ustruct.unpack('<hhh',data)
            print('Eul Ang:' + str(tuple(i/16 for i in angles)))
            
    ## Retrieves the Euler velocity values.
    #
    #  To do this, the 6 bytes of memory storing the Euler velocity values are read.  Then
    #  the 6 bytes are unpacked into 3 numbers corresponding to the 3 axes. These are then
    #  divided by 16 to fix the resolution to degrees/second. The final Euler velocities are returned as a tuple.    
    def get_ang_vel(self):
            data = i2c.mem_read(6, 0x28,0x14)  
            ang_vel = ustruct.unpack('<hhh',data)
            print('Eul Vel:' + str(tuple(i/16 for i in ang_vel)))
    ## Checks the calibration status of the sensor.
    #        
    #  First the byte corresponding the the calibration status is read. Then byte is broken down from hex into binary.
    #  This 8 long binary number is used to check the calibration of 4 different parts in the order of: the whole system,
    #  the gyroscope, the accelerometer, and lastly the magnetometer.  Each of the 4 statuses ranges
    #  from 0 to 3 based on its 2 binary numbers. Thus, a (3,3,3,3) means the sensor is fully calibrated.
    def check_calibration(self):
            data = i2c.mem_read(1, 0x28,0x35)
            status = int.from_bytes(data, 'little')
            status = '{:08b}'.format(status)
            result.clear()
            if int(status[0]) and int(status[1]) == 1:
                result.insert(1,3)
            elif int(status[0]) == 1:
                result.insert(1,2)
            elif int(status[1]) == 1:
                result.insert(1,1)
            else:
                result.insert(1,0)
            if int(status[2]) and int(status[3]) == 1:
                result.insert(2,3)
            elif int(status[2]) == 1:
                result.insert(2,2)
            elif int(status[3]) == 1:
                result.insert(2,1)
            else:
                result.insert(2,0)                
            if int(status[4]) and int(status[5]) == 1:
                result.insert(3,3)
            elif int(status[4]) == 1:
                result.insert(3,2)
            elif int(status[5]) == 1:
                result.insert(3,1)
            else:
                result.insert(3,0) 
            if int(status[6]) and int(status[7]) == 1:
                result.insert(4,3)
            elif int(status[6]) == 1:
                result.insert(4,2)
            elif int(status[7]) == 1:
                result.insert(4,1)
            else:
                result.insert(4,0)
            print('Calib:' + str(result))

#  Creaing the sensor object and then enabling it for testing. 
sensor = bno055()
sensor.enable()

# Main loop to show the function of the BNO055 IMU sensor is working. It simply finds the angles,
# finds the Euler velocities, checks the calibration satus, waits 100 ms, then repeats indefinitely.
# outputting data. 
if __name__ == '__main__':
     while(True):
         sensor.get_angles()
         sensor.get_ang_vel()
         sensor.check_calibration()
         utime.sleep_ms(100)
         

