## @file Lab_3.py
#
#  @section sec_purpose Purpose
#  In this lab, the main purpose was to create a proportional controller to operate the motor and test the effects of
#  different gains on the system.  The user is able to input a gain as well as a setpoint for the motor to rotate towards. THen the motor will run
#  the test and print the test data in text form for the user to evaluate.
#
#  @section sec_usage Usage
#  The main method of code orgainazition for this lab was 3 classes: the MotorDriver, the Encoder, and the Pcontroller class.
#  The motor driver and encoder class allowed setup and usage of the motor and encoder. The Pcontroller class is where the
#  the calculation of error occurs and where it is multiplied by the gain to get the new duty cycle.  A delay of 10 ms also
#  occurs to allow the motor to run for a while before the next test.  
#
#  @section sec_testing Testing
#  In order to ensure the proportional controller was working properly, it was tested by inputing various gains and setpoints.
#  The larger gains had a quicker response while the lower gains had a faster response. Overall, the working gain ranged from 0.02
#  to 2. Any lower and the motor would not run, and any higher the motor would bounce back
#  indefinitely around the setpoint.
#
#  @section sec_graph Step Response Motor Gain Comparison
#  @image html test_data.png 
#  @image html image_test_data.png width=50%
#
#  @section sec_video Gain Test Video
#  https://drive.google.com/file/d/1wRX1ihqEYVddzAHhSZBIVrOe5SSQfDDa/view?usp=sharing
#
#  @section sec_bugs Bugs and Limitations
#  One major limitation of this system is that the motors do not activate under about a duty cycle of 8. This makes it difficult
#  to properly reach a setpoint. This leads to many lower gains not reaching the setpoint as they finish with a non-zero duty cycle
#  that isn't activating the motor to move. 
#
#  @section sourcecode Sourcecode 
#  Sourcode can be found @ https://bitbucket.org/andrewmeyenberg/me405_labs/src/master/Doxygen/Lab_3.py
#
#  @author Andrew Meyenberg
#
#  @date May 13, 2020
#
#  @package motor
#  Micro Metal Gearmotor 50.1 w/ Encoder
#
#  This is the motor and encoder that was used.
#  SKU:FIT0482
#
#  @author Andrew Meyenberg 
#
#  @date May 6, 2020

import pyb
import sys
import utime
'''must import pyb module to to interface with board
   must import sys module to exit file when program is done
   must import utime module to add a predictable delay'''
 
n = 0
err1 = 0
duty = 0

# Includes the pin objects used for interfacing with the the encoders.  
pin_B6 = pyb.Pin (pyb.Pin.cpu.B6, pyb.Pin.AF_PP, af=2)
pin_B7 = pyb.Pin (pyb.Pin.cpu.B7, pyb.Pin.AF_PP, af=2)

# Includes the timer objects used for interfacing with the the encoders. 
tim4 = pyb.Timer(4,prescaler=0, period=65535)
t4ch1 = tim4.channel(1, pyb.Timer.ENC_A, pin=pin_B6)
t4ch2 = tim4.channel(2, pyb.Timer.ENC_B, pin=pin_B7)

# Create the pin objects used for interfacing with the motor driver
pin_EN = pyb.Pin (pyb.Pin.cpu.A10, pyb.Pin.OUT_PP);
pin_IN1 = pyb.Pin (pyb.Pin.cpu.B4, pyb.Pin.OUT_PP);
pin_IN2 = pyb.Pin (pyb.Pin.cpu.B5, pyb.Pin.OUT_PP);

# Create the timer object used for PWM generation
tim = pyb.Timer(3, freq = 20000);
t3ch1 = tim.channel(1, pyb.Timer.PWM, pin=pin_IN1)
t3ch2 = tim.channel(2, pyb.Timer.PWM, pin=pin_IN2)

## A Motor Driver object
#
#  A class used to assign attributes of the motor
#  @author Andrew Meyenberg
#  @date May 13, 2020
class MotorDriver:
    
    ## Constructor for motor driver
    #
    #  Creates a motor driver by initializing GPIO 
    #  pins and turning the motor off for safety.
    #  @param EN_pin A pyb.Pin object to use as the enable pin.
    #  @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
    #  @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
    #  @param timer A pyb.Timer object to use for PWM generation on IN1_pin
    #  and IN2_pin. 
    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer):
        self.EN_pin = EN_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = tim
    print ('Creating a motor driver')
   
    ## Enables the motor 
    #
    #  Enables the motor to run by setting the EN pin high
    def enable (self):
        pin_EN.high ()
        print ('Enabling Motor')

    ## Disables the motor and exits program
    #
    #  In order to disable the motor, the EN pin is set to low. Then a
    #  system exit occurs to end the code when a leave is entered
    def disable (self):
        print('Disabling Motor')
        pin_EN.low ()
        sys.exit()

    ## Sets the duty cycle of the motor
    #
    #  This method sets the duty cycle to be sent
    #  to the motor to the given level. Positive values
    #  cause effort in one direction, negative values
    #  in the opposite direction.  If the absolute value of a
    #  duty cycle is greater than 100, it will be lowered
    #  to 100 and then set.
    #  @param duty A signed integer holding the duty
    #  cycle of the PWM signal sent to the motor    
    def set_duty (self, duty):
        if duty >= 100:
            duty = 100
            pin_IN2.low ()
            t3ch1.pulse_width_percent(int(duty))
            t3ch2.pulse_width_percent(0)
        elif duty >= 0:
            pin_IN2.low ()
            t3ch1.pulse_width_percent(int(duty))
            t3ch2.pulse_width_percent(0)
        elif duty <= -100:
            duty = -100
            pin_IN1.low ()
            t3ch2.pulse_width_percent(int(duty*-1))
            t3ch1.pulse_width_percent(0)
        else:
            pin_IN1.low ()
            t3ch2.pulse_width_percent(int(duty*-1))
            t3ch1.pulse_width_percent(0)

## An Encoder Reader object
#
#  A class used to assign attributes of the Encoder
#  @author Andrew Meyenberg
#  @date May 6, 2020
class Encoder:
    
    ## Constructor for Encoder 
    #
    #  Creates a encoder driver by initializing the timer 
    #  pins a creating attributes to store data
    #  @param pin_A  A pyb.Pin object to be connected to the A phase of the encoder
    #  @param pin_B  A pyb.Pin object to be connected to the B phase of the encoder 
    #  @param timer A pyb.Timer object to use for reading the encoder values 
    #  @param name An attribute of the encoder to be used for naming specific encoders
    #  @param delta  An attribute describing the change in rotation of the encoder between time intervals
    #  @param position An attribute describing the total rotation of the encoder
    #  @param xold An attribute used to hold the position of the encider from the previous time interval to calculate delta
    #  @param xnew An attrubute used to hold the new postion on the encoder in order to calculate delta
    def __init__ (self, pin_A, pin_B, timer, name, delta, position, xold, xnew):
        self.Pin_A = pin_A
        self.pin_B = pin_B
        self.timer = timer
        self.name = name
        self.delta = delta
        self.position = position
        self.xold = xold
        self.xnew = xnew
        print ('Encoder Inititialized')

    ## Updates the encoder 
    #
    #  First, the previous xnew is saved as xold, then the xnew is updated by the timer of that encoder.
    def update(self):
        self.xold = self.xnew
        timer = getattr(self, 'timer') 
        self.xnew = timer.counter()

    ## Obtains the current position of the encoder
    #
    #  The position is udpated ever time interval in order to obtain the total rotational displacement of the encoder.
    #  It does this by taking the old position of the encoder and adding the delta to it. 
    def get_position(self):
        self.position = self.position + self.delta
 
    ## Manually sets the position value of the encoder
    #
    #  The position value is udpated to the specified new position. The motor itself does not move to the position
    #  @param new_position A user specified integer value to become the new current position of the encoder 
    def set_position(self, new_position):
            self.position = new_position

    ## Obtains the change in position, delta
    #
    #  The position is udpated ever time interval in order to obtain the total rotational displacement of the encoder.
    #  It does this by taking the old position of the encoder and adding the delta to it.         
    def get_delta(self):
            self.delta = self.xnew - self.xold
            if self.delta >= 32768:
                self.delta = self.delta - 65535 
            elif self.delta <= -32768:
                self.delta = self.delta + 65535
            else:
                pass         
'''above, is the overflow protection for the encoder.  It ensures that when the encoder overflows,
   it is offset by the max value to get the correct value'''

## A Proportional Controller object
#
#  A class used to assign attributes of the Proportional Controller
#  @author Andrew Meyenberg
#  @date May 13, 2020
class Pcontroller:
    
    ## Constructor for proportional controller 
    #
    #  Creates a proportional controller  by 
    #  creating attributes to store data
    #  @param Kp An attribute of the controller that stores the gain for the test
    #  @param setpoint  An attribute to store the setpoint of the gain test
    def __init__ (self, Kp, setpoint):
        self.Kp = Kp
        self.setpoint = setpoint
        print('Proportional controller intialized')
    
    ## Updates the proportional controller and duty cycle of the motor
    #
    #  First, the position of the encoder is updated. then the delta is found and added to the
    #  total position. Following that, the error from the setpoint is calculated and multiplied
    #  by the gain to set the duty cycle.  
    def update(self):
        enc1.update()
        enc1.get_delta()
        enc1.get_position()
        err1 = setpoint - enc1.position
        duty = (err1*Kp)/1000
        moe.set_duty(int(duty))

# Create a motor object passing in the pins and timer
moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)
# Enable the motor driver
moe.enable()
# Set the duty cycle to 10 percent
moe.set_duty(0)

# Create the two encoder objects to be tested.  Position are set to arbitrary initial values to test.
enc1 = Encoder(pin_B6, pin_B7, tim4,'Encoder 1',0,0,0,0)
enc1.set_position(0)

#  The user inputed values to be used when creating the propotional controller
Kp = int(input('Select a gain for the motor Kp = 0.001 x '))
setpoint =int(input('Select a setpoint for the motor: '))

test_period = 200
'''multiplied by 10ms'''

# Create the two proportional controller object to be tested.  
pc = Pcontroller(Kp, setpoint)

# Creates the array to store the test data of the motor gain test
test_data = [[0 for i in range(2)] for j in range(test_period)]

# Test loop to show the function of the proportional controller is working as well as
# outputting data. 
if __name__ == '__main__':
    while(True):
        if n <= test_period-1:  
            pc.update()
            utime.sleep_ms(10)
            test_data[n][0] = enc1.position
            test_data[n][1] = n
            n = n + 1
        else:
            print(test_data)
            print('Motor gain test complete')
            moe.disable()
            sys.exit()

