## @file Lab_1.py
#
#  @section sec_purpose Purpose
#  In this lab, the main goal was to use the motor driver in conjuction with our main board to drive
#  a motor.  The motor driver allows the user to set a duty cycle of [-100]
#  to [100], which is full power reverse to full power fowrward. It also demostrates a
#  basic user interface and error handling as
#  well as cooperation between the motor driver and main board.
#
#  @section sec_usage Usage
#  To do this, pulse width modulation was used to drive the motor.  In order to drive it in
#  reverse, the PWM and negative end of the motor were programed to switch.  All of this is handle, as well as
#  user input and error messages so a user may input a duty cycle into the console.
#
#  @section sec_mot Motor Driver
#  The motor driver being used to drive the \ref motor in this lab is the X-NUCLEO-IMH04A1.  It is capable
#  of driving two brushed dc motors.  More information on the motor driver can be found @
#  https://www.mouser.com/ProductDetail/STMicroelectronics/X-NUCLEO-IHM04A1?qs=%2Fha2pyFadugfOa1q%2FRFISeT05v4cFA%2Fg264X%2Ft7U2I%2F1jP900NOOjg%3D%3D
#
#  @section sec_testing Testing
#  In order to ensure the motor driver was working properly, it was set to varying levels of pulse width modulation. Both negative and
#  positive values were entered in random order.  Values outside of the range were tested and even invalid inputs. 
#  
#  @section sec_bugs Bugs and Limitations
#  Through my testing, I was unable to come up with any bugs to be concerned about. The limitation of the driver
#  seemed to be that any value under around 8, either positive or negative, did not result in any motion.
#  This can be assumed to be from the friction of the motor preventing low speeds. 
#
#  @section sec_sourcecode Sourcecode
#  Sourcode can be found @ https://bitbucket.org/andrewmeyenberg/me405_labs/src/master/Doxygen/Lab_1.py
#
#
#  @author Andrew Meyenberg
#
#  @copyright License Info
#
#  @date April 24, 2020
#
#  @package motor
#  Micro Metal Gearmotor 50.1 w/ Encoder
#
#  This is the motor that will be driven uses the motor driver
#  SKU:FIT0482
#
#  @author Andrew Meyenberg 
#
#  @copyright License Info
#
#  @date April 24, 2020

import pyb
import sys
'''must import sys module to exit file when program is done'''

# Create the pin objects used for interfacing with the motor driver
pin_EN = pyb.Pin (pyb.Pin.cpu.A10, pyb.Pin.OUT_PP);
pin_IN1 = pyb.Pin (pyb.Pin.cpu.B4, pyb.Pin.OUT_PP);
pin_IN2 = pyb.Pin (pyb.Pin.cpu.B5, pyb.Pin.OUT_PP);

# Create the timer object used for PWM generation
tim = pyb.Timer(3, freq = 20000);
t3ch1 = tim.channel(1, pyb.Timer.PWM, pin=pin_IN1)
t3ch2 = tim.channel(2, pyb.Timer.PWM, pin=pin_IN2)


## A Motor Driver object
#
#  A class used to assign attributes of the motor
#  @author Your Name
#  @copyright License Info
#  @date January 1, 1970
class MotorDriver:
    
    ## Constructor for motor driver
    #
    #  Creates a motor driver by initializing GPIO 
    #  pins and turning the motor off for safety.
    #  @param EN_pin A pyb.Pin object to use as the enable pin.
    #  @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
    #  @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
    #  @param timer A pyb.Timer object to use for PWM generation on IN1_pin
    #  and IN2_pin. 
    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer):
        self.EN_pin = EN_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = tim
    print ('Creating a motor driver')
   
    ## Enables the motor 
    #
    #  Enables the motor to run by setting the EN pin high
    def enable (self):
        pin_EN.high ()
        print ('Enabling Motor')

    ## Disables the motor and exits program
    #
    #  In order to disable the motor, the EN pin is set to low. Then a
    #  system exit occurs to end the code when a leave is entered
    def disable (self):
        print('Disabling Motor')
        pin_EN.low ()
        print('Thanks for using the Motor Driver')
        sys.exit()

    ## Sets the duty cycle of the motor
    #
    #  This method sets the duty cycle to be sent
    #  to the motor to the given level. Positive values
    #  cause effort in one direction, negative values
    #  in the opposite direction.  If the absolute value of a
    #  duty cycle is greater than 100, it will be lowered
    #  to 100 and then set.
    #  @param duty A signed integer holding the duty
    #  cycle of the PWM signal sent to the motor    
    def set_duty (self, duty):
        if duty >= 100:
            duty = 100
            pin_IN2.low ()
            t3ch1.pulse_width_percent(int(duty))
            t3ch2.pulse_width_percent(0)
            print('Duty cycle updated to: ' + str(duty))
        elif duty >= 0:
            pin_IN2.low ()
            t3ch1.pulse_width_percent(int(duty))
            t3ch2.pulse_width_percent(0)
            print('Duty cycle updated to: ' + str(duty))
        elif duty <= -100:
            duty = -100
            pin_IN1.low ()
            t3ch2.pulse_width_percent(int(duty*-1))
            t3ch1.pulse_width_percent(0)
            print('Duty cycle updated to: ' + str(duty))
        else:
            pin_IN1.low ()
            t3ch2.pulse_width_percent(int(duty*-1))
            t3ch1.pulse_width_percent(0)
            print('Duty cycle updated to: ' + str(duty))


# Create a motor object passing in the pins and timer
moe = MotorDriver(pin_EN, pin_IN1, pin_IN2, tim)

# Enable the motor driver
moe.enable()

# Set the duty cycle to 10 percent
moe.set_duty(0)

## String containing user input of a duty cycle to be used to set the PWM of the motor. 
#  The input is first stripped of any negative sign it might have, and checks if it is an integer. If so it that value will set the used to set the duty cycle.
#  Entering a "leave" will cause the motor to come to a stop and exit the console.
user_in = input('Select a duty cycle for the motor or (leave): ')

if __name__ == '__main__':
     while(True):
         if user_in.lstrip('-').isdigit():          
             moe.set_duty(float(user_in))
         elif user_in == 'leave':
             moe.disable()
         else: 
             print('not a valid imput')
         user_in = input('Select a duty cycle for the motor or (leave): ')









