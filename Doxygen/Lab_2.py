## @file Lab_2.py
#
#  @section sec_purpose Purpose
#  In this lab, the main purpose was to use the motor encoder to track and record the change in rotational displacement
#  To do this, the encoder must be powered with 5 V, and the A and B phase must be connected to a timer.  
#  The timer is then able to differentiate between the two phases because they are in quadrature, which allows
#  detection of motion in either direction.
#
#  @section sec_usage Usage
#  Furthermore, through using object attributes, the code is able
#  to handle multiple encoders at once. It does that by storing the encoders positional data
#  as an attribute to the created encoder object. The data can then by access and updated based on
#  how the encoder moves.
#
#  @section sec_testing Testing
#  In order to ensure the encoders were working properly, they were tested manually to see if their values were
#  accurate. This was done by manually spinning the motors and checking if the position and delta of the encoder are
#  updated correctly.  I also tested one of the encoders while the motor was running to ensure higher speeds did not
#  mess up the values. 
#
#
#  @section sec_bugs Bugs and Limitations
#  Through my testing, I have came upon a few critical bugs that still exist.  The encoders will be working fine
#  then all the sudden it will be unresponsive randomly.
#  For example, if I allow the motor to run and measure the delta, it only works sometimes.  It will either give
#  me what I believe to be there correct value, or it will return a value of -1 or 1. This happens randomly as far as I can tell.
#  Furthermore, my second encoder can sometimes not update a negative value.  Again this only happens some of the time and
#  the rest of the time, the functionality works. Also because the code for encoders one and two are nearly identical,
#  it leads me to believe there might be something wrong with the encoder or board. 
#
#  @section sourcecode Sourcecode 
#  Sourcode can be found @ https://bitbucket.org/andrewmeyenberg/me405_labs/src/master/Doxygen/Lab_2.py
#
#
#  @author Andrew Meyenberg
#
#  @date May 6, 2020
#
#  @package motor
#  Micro Metal Gearmotor 50.1 w/ Encoder
#
#  This is the motor and encoder that was used.
#  SKU:FIT0482
#
#  @author Andrew Meyenberg 
#
#  @date May 6, 2020

import pyb
import time
'''must import pyb module to to interact with board and the time module
   for the sleep function'''


## A Encoder Reader object
#
#  A class used to assign attributes of the Encoder
#  @author Andrew Meyenberg
#  @date May 6, 2020
class Encoder:
    
    ## Constructor for Encoder 
    #
    #  Creates a encoder driver by initializing the timer 
    #  pins a creating attributes to store data
    #  @param pin_A  A pyb.Pin object to be connected to the A phase of the encoder
    #  @param pin_B  A pyb.Pin object to be connected to the B phase of the encoder 
    #  @param timer A pyb.Timer object to use for reading the encoder values 
    #  @param name An attribute of the encoder to be used for naming specific encoders
    #  @param delta  An attribute describing the change in rotation of the encoder between time intervals
    #  @param position An attribute describing the total rotation of the encoder
    #  @param xold An attribute used to hold the position of the encider from the previous time interval to calculate delta
    #  @param xnew An attrubute used to hold the new postion on the encoder in order to calculate delta
    def __init__ (self, pin_A, pin_B, timer, name, delta, position, xold, xnew):
        self.Pin_A = pin_A
        self.pin_B = pin_B
        self.timer = timer
        self.name = name
        self.delta = delta
        self.position = position
        self.xold = xold
        self.xnew = xnew
        print ('Encoder Inititialized')

    ## Updates the encoder 
    #
    #  First, the previous xnew is saved as xold, then the xnew is updated by the timer of that encoder.
    def update(self):
        self.xold = self.xnew
        timer = getattr(self, 'timer') 
        self.xnew = timer.counter()

    ## Obtains the current position of the encoder
    #
    #  The position is udpated ever time interval in order to obtain the total rotational displacement of the encoder.
    #  It does this by taking the old position of the encoder and adding the delta to it. 
    def get_position(self):
        self.position = self.position + self.delta
        print('position of ' + self.name + ' = ' + str(self.position))
 
    ## Manually sets the position value of the encoder
    #
    #  The position value is udpated to the specified new position. The motor itself does not move to the position
    #  @param new_position A user specified integer value to become the new current position of the encoder 
    def set_position(self, new_position):
            self.position = new_position
            print('New position of' +self.name + '=' + str(new_position))

    ## Obtains the change in position, delta
    #
    #  The position is udpated ever time interval in order to obtain the total rotational displacement of the encoder.
    #  It does this by taking the old position of the encoder and adding the delta to it.         
    def get_delta(self):
            self.delta = self.xnew- self.xold
            if self.delta >= 32768:
                self.delta = self.delta - 65535 
                print('delta of ' + self.name + ' = ' + str(self.delta))
            elif self.delta <= -32768:
                self.delta = self.delta + 65535
                print('delta of ' + self.name + ' = ' + str(self.delta))
            else:
                print('delta of ' + self.name + ' = ' + str(self.delta))         
'''above, is the overflow protection for the encoder.  It ensures that when the encoder overflows,
   it is offset by the max value to get the correct value'''

# Includes the pin objects used for interfacing with the the encoders.  Pins B6 and B7
# correspond to the first encoder and pins C6 and C7 correspond to the second encoder
pin_B6 = pyb.Pin (pyb.Pin.cpu.B6, pyb.Pin.AF_PP, af=2)
pin_B7 = pyb.Pin (pyb.Pin.cpu.B7, pyb.Pin.AF_PP, af=2)
pin_C6 = pyb.Pin (pyb.Pin.cpu.C6, pyb.Pin.AF_PP, af=3)
pin_C7 = pyb.Pin (pyb.Pin.cpu.C7, pyb.Pin.AF_PP, af=3)

# Includes the timer objects used for interfacing with the the encoders. Tim4
# correspond to the first encoder's timer and Tim8 correspond to the second encoder's timer
tim4 = pyb.Timer(4,prescaler=0, period=65535)
tim8 = pyb.Timer(8,prescaler=0, period=65535)
t4ch1 = tim4.channel(1, pyb.Timer.ENC_A, pin=pin_B6)
t4ch2 = tim4.channel(2, pyb.Timer.ENC_B, pin=pin_B7)
t8ch1 = tim8.channel(1, pyb.Timer.ENC_A, pin=pin_C6)
t8ch2 = tim8.channel(2, pyb.Timer.ENC_B, pin=pin_C7)

# Create the two encoder objects to be tested.  Position are set to arbitrary initial values to test.
enc1 = Encoder(pin_B6, pin_B7, tim4,'Encoder 1',0,0,0,0)
enc1.set_position(0)
enc2 = Encoder(pin_C6, pin_C7, tim8,'Encoder 2',0,0,0,0)
enc2.set_position(-100)

# Test loop to show the function of the encoder is operational. The time sleep value determines the time
# interval that seperates each check of the new position.
if __name__ == '__main__':
    while(True):
        time.sleep(.1)
        enc1.update()
        enc1.get_delta()
        enc1.get_position()
        enc2.update()
        enc2.get_delta()
        enc2.get_position()

        
        






